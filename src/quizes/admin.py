from django.contrib import admin

from .models import Question, AnswerChoices, Quiz


class AnswersInline(admin.TabularInline):
    model = AnswerChoices


class QuestionAdmin(admin.ModelAdmin):
    inlines = (AnswersInline,)


admin.site.register(Question, QuestionAdmin)


class QuestionsInline(admin.TabularInline):
    model = Question


class QuizAdmin(admin.ModelAdmin):
    inline = [QuestionsInline]


admin.site.register(Quiz, QuizAdmin)
