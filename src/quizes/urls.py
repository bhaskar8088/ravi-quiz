from django.conf.urls import url

from .views import Test, Result, ManageQuiz, AllQuiz, AddQuiz


urlpatterns = [
    url(r'^quiz/(?P<quiz_id>[0-9]+)', Test.as_view(), name="quiz"),
    url(r'^result/(?P<quiz_id>[0-9]+)', Result.as_view(), name="result"),
    url(r'^quiz/$', AllQuiz.as_view(), name="all"),
    url(r'^quiz/edit/(?P<quiz_id>[0-9]+)$', ManageQuiz.as_view(), name="edit"),
    url(r'^quiz/add/$', AddQuiz.as_view(), name="add"),
]
