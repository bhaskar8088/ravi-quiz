from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


QUESTION_TYPES = (
    (1, 'Multiple Choice'),
    (2, 'Descriptive')
)


class Quiz(models.Model):
    title = models.CharField(max_length=40)

    start = models.DateTimeField()
    endtime = models.DateTimeField()

    def is_open(self):
        return self.start < timezone.now() < self.endtime

    def has_started(self):
        return self.start < timezone.now()

    def has_ended(self):
        return timezone.now() > self.endtime

    def status(self):
        if self.is_open():
            return 'Open'
        if self.has_ended():
            return 'Closed on %s' % (self.endtime.strftime("%B %d, %Y"))

        return 'Starts on %s' % (self.start.strftime("%B %d, %Y"))

    def __str__(self):
        return self.title


class Question(models.Model):
    value = models.TextField()
    q_type = models.IntegerField(choices=QUESTION_TYPES, default=1)

    in_quiz = models.ForeignKey(Quiz)

    def __str__(self):
        return self.value

    def get_answers(self):
        return self.answerchoices_set.filter(correct=True)


class AnswerChoices(models.Model):
    value = models.CharField(max_length=20)
    correct = models.BooleanField(default=False)
    to_question = models.ForeignKey(Question)

    def __str__(self):
        return self.value
