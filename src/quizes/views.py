from datetime import datetime

from django.views.generic import View
from django.template.response import TemplateResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator
from django.utils import timezone

from .models import Quiz, Question, AnswerChoices
from schools.models import TestDetails


def is_admin(user):
    return user.is_authenticated() and user.profile.user_type == 'adm'


class Test(View):

    def get(self, request, quiz_id):
        if not request.user.is_authenticated():
            return TemplateResponse(request, 'schools/login-reg.html')

        profile = request.user.profile
        if profile.is_staff():
            return TemplateResponse(request, 'schools/test-dashboard.html')

        if profile.is_student():
            quiz = get_object_or_404(Quiz, id=quiz_id)
            if profile.has_participated(quiz):
                return TemplateResponse(
                    request,
                    'prepared/message-alert.html',
                    {'message': 'You have already participated in the quiz.'})

            return TemplateResponse(request,
                                    'quizes/quiz.html',
                                    {'quiz': quiz,
                                     'questions': quiz.question_set.all()})

        return TemplateResponse(request, 'schools/home.html')

    def post(self, request, quiz_id):
        if not request.user.is_authenticated():
            return TemplateResponse(request, 'schools/login-reg.html')

        profile = request.user.profile
        if profile.is_staff():
            return TemplateResponse(request, 'schools/test-dashboard.html')

        if profile.is_student():
            quiz = get_object_or_404(Quiz, id=quiz_id)
            if profile.has_participated(quiz):
                return TemplateResponse(
                    request,
                    'prepared/message-alert.html',
                    {'message': 'You have already participated in the quiz.'})

            user = request.user.profile
            try:
                test = TestDetails.objects.create(student=user, quiz=quiz)

                for question in quiz.question_set.all():
                    ans_choice = request.POST.get(
                        'for_q_%d' % (question.id), None)
                    question.testanswers_set.create(
                        test=test,
                        answer=AnswerChoices.objects.get(id=ans_choice))

                return TemplateResponse(
                    request,
                    'prepared/message-success.html',
                    {'message': 'Your answers have been saved successfully.'})
            except Exception as e:
                print '%s (%s)' % (e.message, type(e))
                if quiz is not None:
                    quiz.delete()

                return TemplateResponse(
                    request, 'quizes/add.html',
                    {'quiz': quiz,
                     'message': {'type': 'warning', 'head': 'Alert!!',
                                 'value': 'Could not save the quiz'}})


class Result(View):

    def get(self, request, quiz_id):
        if not request.user.is_authenticated():
            return TemplateResponse(request, 'schools/login-reg.html')

        profile = request.user.profile
        if profile.is_staff():
            return TemplateResponse(request, 'schools/test-dashboard.html')

        if profile.is_student():
            quiz = get_object_or_404(Quiz, id=quiz_id)
            if not profile.has_participated(quiz):
                return TemplateResponse(
                    request,
                    'prepared/message-alert.html',
                    {'message': 'You have not participated in the quiz yet.'})

            test = profile.testdetails_set.filter(quiz=quiz).first()
            answers = test.answers.all()
            total = test.answers.count()
            marks = len([a for a in answers if a.is_correct()])

            return TemplateResponse(request,
                                    'quizes/results.html',
                                    {'quiz': quiz,
                                     'answers': answers,
                                     'total': total,
                                     'marks': marks})

        return TemplateResponse(request, 'schools/home.html')


class AllQuiz(View):

    @method_decorator(user_passes_test(is_admin))
    def get(self, request):
        quizes = Quiz.objects.all()
        return TemplateResponse(
            request, 'quizes/manage.html', {'quizes': quizes})


class AddQuiz(View):

    @method_decorator(user_passes_test(is_admin))
    def get(self, request):
        return TemplateResponse(request, 'quizes/add.html')

    def post(self, request):
        quiz = None
        # import pdb; pdb.set_trace()

        try:
            title = request.POST['title']
            print(request.POST)
            start_date = request.POST['start_date']
            start_time = request.POST['start_time']
            end_date = request.POST['end_date']
            end_time = request.POST['end_time']

            starttime = timezone.make_aware(datetime.strptime(
                start_date + ' ' + start_time, '%Y-%m-%d %I:%M %p'))
            endtime = timezone.make_aware(datetime.strptime(
                end_date + ' ' + end_time, '%Y-%m-%d %I:%M %p'))
            quiz = Quiz.objects.create(title=title,
                                       start=starttime, endtime=endtime)

            # print(request.POST.getlist('question'))

            # questions = request.POST['question']
            i = j = 0

            q = request.POST.get('question[%d]' % (i), '')
            while q != '':
                print('Question: ' + q)
                question = Question.objects.create(
                                value=q, q_type=1, in_quiz=quiz)

                a = request.POST.get('option[%d][%d]' % (i, j), '')
                c = bool(request.POST.get('correct[%d][%d]' % (i, j), False))
                while a != '':
                    print('Answer: ' + a)
                    AnswerChoices.objects.create(
                        value=a, correct=c, to_question=question)
                    j += 1
                    a = request.POST.get('option[%d][%d]' % (i, j), '')
                    c = bool(request.POST.get('correct[%d][%d]' % (i, j),
                                              False))

                j = 0
                i += 1
                q = request.POST.get('question[%d]' % (i), '')

            # print(questions)
            # for q in questions:
            #     print(q)

            quizes = Quiz.objects.all()
            return TemplateResponse(
                request, 'quizes/manage.html',
                {'message': {'type': 'success', 'head': 'Success!!',
                             'value': 'Quiz saved successfully'},
                 'quizes': quizes})

        except Exception as e:
            print '%s (%s)' % (e.message, type(e))
            if quiz is not None:
                quiz.delete()

            return TemplateResponse(
                request, 'quizes/add.html',
                {'message': {'type': 'warning', 'head': 'Alert!!',
                             'value': 'Could not save the quiz'}})


class ManageQuiz(View):

    @method_decorator(user_passes_test(is_admin))
    def get(self, request, quiz_id):
        quiz = get_object_or_404(Quiz, id=quiz_id)
        return TemplateResponse(
            request, 'quizes/manage-quiz.html', {'quiz': quiz})

    @method_decorator(user_passes_test(is_admin))
    def post(self, request):
        pass
