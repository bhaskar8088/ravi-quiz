angular
    .module('puzzled.main')
    .controller('AddQuizController', [function()
    {
        var vm = this;

        vm.title = '';
        vm.questions = [];
        vm.question = {value: ''};
        vm.answer = {value: '', correct: false}; 
        vm.startdatepicker = {};
        vm.enddatepicker = {};

        vm.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        console.log('asdf');

        vm.addQuestion = function()
        {
            var question = angular.extend({}, vm.question);
            question.options = [];
            for(var i=0; i<4; i++)
            {
                question.options.push(angular.extend({},vm.answer));
            }
            vm.questions.push(question);
        }

        vm.addOption = function(question)
        {
            question.options.push(angular.extend({},vm.answer));
        }

        vm.openStart = function()
        {
            vm.startdatepicker.opened = true;
        }

        vm.openEnd = function()
        {
            vm.enddatepicker.opened = true;
        }
    }]);
