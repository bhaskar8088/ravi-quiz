# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quizes', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_type', models.CharField(max_length=10, choices=[('staff', 'Staff'), ('stud', 'Student')])),
                ('standard', models.IntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('city', models.CharField(max_length=40, choices=[('hyd', 'Hyderabad'), ('mum', 'Mumbai'), ('del', 'Delhi'), ('ban', 'Banglore')])),
            ],
        ),
        migrations.CreateModel(
            name='TestAnswers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.ForeignKey(to='quizes.AnswerChoices', null=True)),
                ('question', models.ForeignKey(to='quizes.Question')),
            ],
        ),
        migrations.CreateModel(
            name='TestDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('max_marks', models.IntegerField(null=True)),
                ('score', models.IntegerField(null=True)),
                ('quiz', models.ForeignKey(to='quizes.Quiz')),
                ('student', models.ForeignKey(to='schools.Profile')),
            ],
        ),
        migrations.AddField(
            model_name='testanswers',
            name='test',
            field=models.ForeignKey(related_name='answers', to='schools.TestDetails'),
        ),
        migrations.AddField(
            model_name='profile',
            name='in_school',
            field=models.ForeignKey(to='schools.School'),
        ),
        migrations.AddField(
            model_name='profile',
            name='login',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='profile',
            name='tests',
            field=models.ManyToManyField(to='quizes.Quiz', through='schools.TestDetails'),
        ),
    ]
