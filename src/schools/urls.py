from django.conf.urls import url

from .views import Home, Login, Logout, Register, Dashboard, DashboardDetail


urlpatterns = [
    url(r'^$', Home.as_view(), name="home"),
    url(r'^login/$', Login.as_view(), name="login"),
    url(r'^logout/$', Logout.as_view(), name="logout"),
    url(r'^register/$', Register.as_view(), name="register"),
    url(r'^dashboard/$', Dashboard.as_view(), name="dashboard"),
    url(r'^dashboard/(?P<quiz_id>[0-9]+)$',
        DashboardDetail.as_view(), name="dashboard-detail"),
]
