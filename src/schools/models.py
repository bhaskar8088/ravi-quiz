from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from quizes.models import Quiz, Question, AnswerChoices


CITY_CHOICES = (
    ('hyd', 'Hyderabad'),
    ('mum', 'Mumbai'),
    ('del', 'Delhi'),
    ('ban', 'Banglore')
)

USER_TYPE = (
    ('adm', 'Admin'),
    ('staff', 'Staff'),
    ('stud', 'Student')
)


class School(models.Model):
    name = models.CharField(max_length=40)

    city = models.CharField(max_length=40, choices=CITY_CHOICES)

    def __str__(self):
        return '%s, %s' % (self.name, self.city)


class Profile(models.Model):
    login = models.OneToOneField(User)
    in_school = models.ForeignKey(School, null=True)
    user_type = models.CharField(max_length=10, choices=USER_TYPE)
    standard = models.IntegerField(null=True)

    tests = models.ManyToManyField(Quiz, through='TestDetails')

    # def save(self, *args, **kwargs):
    #     self.login = User.objects.create_user(
    #         username=kwargs['username'], password=kwargs['password'])
    #     # self.user_type = 'staff'
    #     # self.in_school = kwargs['in_school']
    #     return super(Profile, self).save(*args, **kwargs)

    def __str__(self):
        return '%s: %s' % (self.user_type, self.login.username)

    def is_staff(self):
        return self.user_type == 'staff'

    def is_student(self):
        return self.user_type == 'stud'

    def is_admin(self):
        return self.user_type == 'adm'

    def has_participated(self, quiz):
        return self.testdetails_set.filter(quiz=quiz).first() is not None

# class Staff(models.Model):
#     login = models.OneToOneField(User)

#     in_school = models.ForeignKey(School)

#     def save(self, *args, **kwargs):
#         group = Group.objects.filter(name='staff').first()
#         if group is None:
#             group = Group.objects.create(name='staff')

#         user = super(Staff, self).save(*args, **kwargs)
#         user.groups.add(group)
#         return user

#     def __str__(self):
#         return 'Staff: %s' % (self.login.username)


# class Student(models.Model):
#     login = models.OneToOneField(User)

#     in_school = models.ForeignKey(School)
#     standard = models.IntegerField()

#     tests = models.ManyToManyField(Quiz, through='TestDetails')

#     def save(self, *args, **kwargs):
#         group = Group.objects.filter(name='student').first()
#         if group is None:
#             group = Group.objects.create(name='student')

#         user = super(Student, self).save(*args, **kwargs)
#         user.groups.add(group)
#         return user

#     def __str__(self):
#         return 'Student: %s, standard: %s' % (self.login.username,
#                                               self.standard)


class TestDetails(models.Model):
    student = models.ForeignKey(Profile)
    quiz = models.ForeignKey(Quiz)

    max_marks = models.IntegerField(null=True)
    score = models.IntegerField(null=True)

    # answers = models.ManyToManyField('TestAnswers')

    def __str__(self):
        return '[Student: %s, Quiz: %s]' % (self.student, self.quiz)


class TestAnswers(models.Model):
    test = models.ForeignKey(TestDetails, related_name="answers")
    question = models.ForeignKey(Question)
    answer = models.ForeignKey(AnswerChoices, null=True)

    def is_correct(self):
        return self.answer in self.question.answerchoices_set.filter(
            correct=True)

    def __str__(self):
        return '[Test: %s, Q: %s, A: %s]' % (
            self.test, self.question, self.answer)
