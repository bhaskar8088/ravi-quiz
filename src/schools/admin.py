from django.contrib import admin
from django.contrib.auth.models import User

from .models import School, Profile, TestDetails, TestAnswers


class ProfileInline(admin.TabularInline):
    model = Profile


class ProfileAdmin(admin.ModelAdmin):
    inlines = (ProfileInline, )


admin.site.register(School)
admin.site.unregister(User)
admin.site.register(User, ProfileAdmin)
# admin.site.register(Staff)
# admin.site.register(Student)
admin.site.register(TestDetails)
admin.site.register(TestAnswers)
