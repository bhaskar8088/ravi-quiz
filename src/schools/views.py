from django.views.generic import View
from django.template.response import TemplateResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404

from quizes.models import Quiz
from .models import School, Profile


class Home(View):
    def get(self, request):
        if not request.user.is_authenticated():
            return TemplateResponse(request, 'schools/login-reg.html')

        profile = request.user.profile
        if profile.is_admin():
            url = reverse('quizes:all')
            return HttpResponseRedirect(url)
        if profile.is_staff():
            url = reverse('schools:dashboard')
            return HttpResponseRedirect(url)
        if profile.is_student():
            quizes = Quiz.objects.all()
            print(quizes)
            return TemplateResponse(request,
                                    'schools/quizes.html',
                                    {'quizes': quizes})

        return TemplateResponse(request, 'schools/home.html')


class Login(View):
    def get(self, request):
        if not request.user.is_authenticated():
            return TemplateResponse(request, 'schools/login-reg.html')
        return HttpResponseRedirect('/')

    def post(self, request):
        if request.user.is_authenticated():
            return HttpResponseRedirect('/')

        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user and user.is_active:
            login(request, user)
            return HttpResponseRedirect('/')

        return TemplateResponse(request, 'schools/login-reg.html')


class Logout(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class Register(View):
    def get(self, request):
        if not request.user.is_authenticated():
            schools = School.objects.all()
            return TemplateResponse(request, 'schools/registration.html', {
                'schools': schools
                })
        return HttpResponseRedirect('/')

    def post(self, request):
        if request.user.is_authenticated():
            return HttpResponseRedirect('/')

        username = request.POST['username']
        password = request.POST['password']

        user = User.objects.create_user(username=username, password=password)
        standard = 0
        try:
            standard = int(float(request.POST.get('standard', '0')))
        except Exception:
            pass

        Profile.objects.create(
            login=user,
            user_type=request.POST['user_type'],
            in_school=School.objects.get(id=request.POST['school']),
            standard=standard)

        return TemplateResponse(request, 'schools/login-reg.html')


class Dashboard(View):
    def get(self, request):
        if not request.user.is_authenticated():
            return TemplateResponse(request, 'schools/login-reg.html')

        profile = request.user.profile
        if profile.is_admin():
            quizes = Quiz.objects.all()
            return TemplateResponse(request, 'quizes/admin-dashboard.html',
                                    {'quizes': quizes})

        if profile.is_staff():
            quizes = Quiz.objects.all()
            school = profile.in_school
            data = []

            for q in quizes:
                item = {}
                item['quiz'] = q
                item['count'] = q.testdetails_set.filter(
                    student__in_school=school).count()
                data.append(item)

            return TemplateResponse(request, 'quizes/teacher-dashboard.html',
                                    {'data': data})


class DashboardDetail(View):
    def get(self, request, quiz_id):
        if not request.user.is_authenticated():
            return TemplateResponse(request, 'schools/login-reg.html')

        profile = request.user.profile
        if profile.is_staff():
            school = profile.in_school
            try:
                # print(quiz_id)
                quiz = get_object_or_404(Quiz, id=quiz_id)
                data = []
                tests = quiz.testdetails_set.filter(
                    student__in_school=school)

                for test in tests:
                    # print(test.student)
                    item = {}
                    item['student'] = test.student
                    item['participated'] = test.student.has_participated(quiz)
                    data.append(item)

                return TemplateResponse(
                    request, 'quizes/teacher-dashboard-detail.html',
                    {'quiz': quiz, 'data': data})

            except Exception as e:
                print '%s (%s)' % (e.message, type(e))

                return TemplateResponse(
                    request, 'quizes/teacher-dashboard-detail.html',
                    {'data': [],
                     'message': {'type': 'warning', 'head': 'Alert!!',
                                 'value': 'Could not get details'}})
