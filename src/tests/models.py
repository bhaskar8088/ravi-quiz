from __future__ import unicode_literals

from django.db import models

from quizes.models import Quiz
from schools.models import Student


class Test(models.Model):
    student = models.ForeignKey(Student)
    quiz = models.ForeignKey(Quiz)
